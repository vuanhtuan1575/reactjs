import { FILTER,GET_PRODUCT, GET_PRODUCTS } from "./actions";

const initialState = {
  products: undefined,
  category: "",
  product: undefined,
  index : -1,
  page : 1
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FILTER:
      return { ...state, category: action.category };
      case GET_PRODUCT : {
        const product = state.products[action.index];
        return {...state, product:product, index: action.index }
      }
      case GET_PRODUCTS: {
        return {...state, products:action.products,page:action.page}
      }
    default:
      return state;
  }
};
export default reducer;
