import React, { Component } from "react";
import { connect } from "react-redux";
import queryString from "query-string";
import { bindActionCreators } from "redux";
import { getProduct } from "../actions";

class DetailPage extends Component {
  componentDidMount() {
    const parsed = queryString.parse(window.location.search);
    this.props.getProduct(parsed.index);
  }
  render() {
    const { product, index } = this.props;
    return (
      <div>
        {product && <img src={product.image} alt={product.name} />}
        {index && <p>index {index}</p>}
        {console.log(index)}
      </div>
    );
  }
}

DetailPage.defaultProps = {
  product: undefined
};
//tao component ProductCard, dua component vao list

const mapStateToProps = state => ({
  product: state.product,
  index: state.index
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ getProduct }, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(DetailPage);
