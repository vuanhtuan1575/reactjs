import React, { Component } from "react";
import ProductCard from "../components/ProductCard.js";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { filter,getProducts } from "../actions";
import SearchBox from "../components/SearchBox"
// import Cart from "../components/Cart"


class ListPage extends Component {
  state={query:''}
  componentDidMount (){
    this.props.getProducts(this.state.query)
  }
  handleLoadmore = ()=>{
    let page = this.props.page;
    page++;
    this.props.getProducts(this.state.query, page)
  }
  handdleSearch = ()=>{
    this.props.getProducts(this.state.query, 1, true)
  }
  handdleChange = event => {
    this.setState({query: event.target.value})
  }
 
  render() {
    const {errMsg, status} = this.props;
    return (
      
      <div>
        <SearchBox></SearchBox>
        
        <button type="button" onClick={() => this.props.filter("apple")}>
          APPLE
        </button>
        <button type="button" onClick={() => this.props.filter("samsung")}>
          SAMSUNG
        </button>
        <p>Selected Category : {this.props.category}</p>
        <div className="container">
          {this.props.products.map((item, index) => (
            <ProductCard 
            product={item} 
            index={index} 
            isSelected={index === this.props.index}/>
          ))}
        </div>
        <button type="button" onClick={this.handleLoadmore} >Load More</button>
      </div>
    );
  }
}
ListPage.defaultProps = {
  products: [],
  category: ""
};
//tao component ProductCard, dua component vao list

const mapStateToProps = state => ({
  products: state.products,
  category: state.category,
  index: state.index,
  page: state.page
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ filter, getProducts }, dispatch)
});
export default connect(mapStateToProps, mapDispatchToProps)(ListPage);
