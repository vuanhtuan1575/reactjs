import { makeProductsApi } from "./apis";


export const FILTER = "FILTER";
export const GET_PRODUCT = "GET_PRODUCT"
export const GET_PRODUCTS = "GET_PRODUCTS"
export const SEARCH = "SEARCH"


export const filter = category => ({
  type: FILTER,
  category: category
});
export const getProduct = index => ({
  type : GET_PRODUCT,
  index
})
export const getProductsSuccess = (products, page) => ({
  type : GET_PRODUCTS,
  products,
  page
})
export const searchBox = value => {
  return dispatch =>{
    console.log("this is running search box:" + value)
    fetch(makeProductsApi((1, value)))
    .then(res => res.json())
    .then(json => {
      //dispatch
      dispatch(getProductsSuccess(json.data))
    })
    .catch (err => console.error(err))
  }
}
export const getProducts = (query, page = 1) =>{
  return dispatch =>{
    fetch(makeProductsApi(page, query))
    .then(res => res.json())
    .then(json => {
      //dispatch
      dispatch(getProductsSuccess(json.data,page))
    })
    .catch (err => console.error(err))
  }
}