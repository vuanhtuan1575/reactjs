import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./ProductCard.css";

class ProductCard extends Component {
  state = {
    isDisplayed: false
  };
  showPrice = () => {
    this.setState({ isDisplayed: true });
  };
  render() {
    const { product, index } = this.props;
    return (
      <div className="product">
        <Link
          to={`/detail?id=${product.id}&name=${product.name}&index=${index}`}
        >
          <div>
            <img src={product.img_url} alt={product.name}></img>
            <h4
              style={{
                backgroundColor: this.props.isSelected && 'yellow'
              }}
            >
              {product.name}
            </h4>
          </div>
        </Link>
        <button type="button" onClick={this.showPrice}>
          {" "}
          Show Price
        </button>
        <button type="button"onClick>Add to Cart</button>
        <h5
          style={{
            textAlign: "center",
            display: this.state.isDisplayed ? "block" : "none"
          }}
        >
          {product.final_price}
        </h5>
      </div>
    );
  }
}

export default ProductCard;
