import React, { Component } from "react";
import HomePage from "./pages/HomePage";
import ListPage from "./pages/ListPage";
import TopMenu from "./components/TopMenu";
import GioHang from "./pages/GioHang"
import { BrowserRouter as Router, Switch, Route, } from "react-router-dom";
import DetailPage from "./pages/DetailPage";
import SearchBox from "./components/SearchBox"





 class App extends Component {
  state = {
    value: "",
    list: []
  }
  render() {
    return (
      
      <React.Fragment>
        <Router>
          <TopMenu></TopMenu>
          <Switch>
            <Route exact path="/cart" component={GioHang}></Route>
            <Route exact path="/" component={HomePage}></Route>
            <Route exact path="/list" component={ListPage}></Route>
            <Route exact path="/detail" component={DetailPage}></Route>
          </Switch>
        </Router>
        <div>
        {/* <SearchBox handleAdd={this.handleAdd} handleChange={this.handleChange}/> */}
        </div>
      </React.Fragment>
    );
  }
}

export default App;
